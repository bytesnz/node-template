module.exports = {
	'{src/README.md,CHANGELOG.md,test/example.js,test/structure.js,package.json}': [
		() => 'repo-utils/mdFileInclude.cjs src/README.md README.md',
		'git add README.md'
	],
	'index.js': ['npm run typings', 'git add index.d.ts'],
	'*.{css,md,json}': ['prettier --write --ignore-path .lintignore'],
	'*.{mjs,cjs,js,ts}': ['prettier --write --ignore-path .lintignore', 'eslint -c eslint.config.commit.mjs --fix']
};
