node-template
========

Template repo for NodeJS projects. Configured for:
* [ESLint][] & [Prettier][] for code formatting and linting
* [lint-staged][] and [husky][] for running linting on git operations
* [gitlab-ci][] jobs for testing, tagging and publishing to [NPM][]
* [repo-utils][] for setting up repo labels, creating Typescript typings from
  JSON and compiling the *src/README.md* file

## To Use

Clone, install dependencies and husky, edit, change to your new repo and
presto.

```sh
read -p 'Please give a new folder name: ' folder && \
git clone -o template --recurse-submodules https://gitlab.com/bytesnz/node-template.git "$folder" && \
cd "$folder" && \
npm install && \
npx husky init
```

...edit, commit, then push...

```sh
read -p 'Please give your git repository URL: ' url && \
git remote add origin "$url" && \
git push --set-upstream origin
```

## Updates

Occassionally there will be updates to this repository. If the update includes
changes to files that you have modified, for example the README.md file,
there may be conflicts.

Updates to files like README.md can be ignored by:

* running the merge command to pull in the other changes from this
  repository
* ignoring changes to files that have been rewritten and adding these to the
  merge commit
* merging any other conflicts
* committing the merge.

```sh
git fetch template
git merge --no-commit template/main
git checkout --ours README.md
git add README.md
...
git commit
```

[husky]: https://typicode.github.io/husky
[eslint]: https://eslint.org/
[git-clone]: https://www.git-scm.com/docs/git-clone
[prettier]: https://prettier.io/
[lint-staged]: https://github.com/okonet/lint-staged#readme
[gitlab-ci]: https://docs.gitlab.com/ee/ci/
[npm]: https://npmjs.com/
[repo-utils]: https://gitlab.com/bytesnz/repo-utils
