import config from './eslint.config.mjs';
import jsdoc from 'eslint-plugin-jsdoc';

export default [
	...config,
	jsdoc.configs['flat/recommended-error'],
	{
		rules: {
			'no-console': [2, { allow: ['warn', 'error'] }],
			'no-debugger': 2,
			'no-warning-comments': [2, { terms: ['xxx', 'todo!!!'], location: 'anywhere' }],
		},
	},
	{
		files: ['examples/**/*.js', 'test/**/*.js'],
		rules: {
			'no-console': 0
		}
	}
];
