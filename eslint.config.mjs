import globals from 'globals';
import path from 'node:path';
import { fileURLToPath } from 'node:url';
import js from '@eslint/js';
import { FlatCompat } from '@eslint/eslintrc';
import { includeIgnoreFile } from '@eslint/compat';
import jsdoc from 'eslint-plugin-jsdoc';
import { resolve } from 'node:path';

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);
const compat = new FlatCompat({
	baseDirectory: __dirname,
	recommendedConfig: js.configs.recommended,
	allConfig: js.configs.all
});

export default [
	includeIgnoreFile(resolve(__dirname, '.lintignore')),
	{
		ignores: ['**/index.d.ts']
	},
	...compat.extends('eslint:recommended', 'prettier'),
	jsdoc.configs['flat/recommended'],
	{
		languageOptions: {
			globals: {
				...globals.node
			},
			ecmaVersion: 2020,
			sourceType: 'module'
		},
		rules: {
			'no-fallthrough': 2,
			'no-case-declarations': 2,
			'no-console': [
				1,
				{
					allow: ['warn', 'error']
				}
			],
			'no-debugger': 1,
			'no-warning-comments': [
				1,
				{
					terms: ['xxx', 'todo', 'fixme', 'todo!!!'],
					location: 'anywhere'
				}
			]
		}
	},
	{
		files: ['test/*.js'],
		rules: {
			'no-console': 0
		}
	}
];
